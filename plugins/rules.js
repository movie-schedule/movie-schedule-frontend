import Vue from 'vue'

export default function() {
  Vue.mixin({
    props: {
      rules: {
        type: Object,
        default: () => ({
          required: [(v) => !!v || 'This field is required'],
          pdf: [
            (v) => (v && v.type.includes('pdf')) || 'Only pdf files are allowed'
          ],
          onechar: (v) => (v && v.length > 1) || '',
          tin_number: [(v) => !v || /^\d{10}$/.test(v) || 'Invalid tin number'],
          email: [
            (v) =>
              !v ||
              /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(
                v
              ) ||
              'Invalid Email Address'
          ],
          password_length: [(v) => !v || v.length >= 8 || 'Password too short'],
          ethiopian_phone_number: [
            (v) =>
              !v || /^\+251\d{9}$/.test(v) || 'Invalid Ethiopian Phone Number '
          ],

          et_phone_number_nullable: [
            (v) =>
              !v ||
              /^\+251\d{0}$/.test(v) ||
              /^\+251\d{9}$/.test(v) ||
              'Invalid Ethiopian Phone Number '
          ],
          et_phone_number_null_check: (v) =>
            !v || /^\+251\d{0}$/.test(v) || false
        })
      }
    }
  })
}
