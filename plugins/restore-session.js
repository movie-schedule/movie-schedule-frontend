export default function ({
  store
}) {
  console.log("reloaded");

  let user = localStorage.getItem('session') || 'null'
  let profile = localStorage.getItem('profile') || 'null'

  user = JSON.parse(user)
  profile = JSON.parse(profile)

  if (user) {
    store.commit('common/login', user)

    if (profile) {
      store.commit('common/set_user_data', profile)
    } else {
      store.dispatch('common/fetch_user_data')
    }
  }
}
