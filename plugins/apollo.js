import Vue from 'vue'
import VueApollo from 'vue-apollo'
import {
  ApolloClient
} from 'apollo-client'
import {
  HttpLink
} from 'apollo-link-http'
import {
  InMemoryCache
} from 'apollo-cache-inmemory'
import {
  split,
  ApolloLink,
  from
} from 'apollo-link'
import {
  WebSocketLink
} from 'apollo-link-ws'
import {
  getMainDefinition
} from 'apollo-utilities'
import {
  onError
} from 'apollo-link-error'

export default function ({
  store,
  app
}) {
  const connectionParams = () => {
    return {
      headers: {
        authorization: store.getters['common/logged_in'] ?
          `Bearer ${store.getters['common/token'] ||
              store.getters['common/token']}` : undefined
      }
    }
  }

  const errorLink = onError(({
    graphQLErrors,
    networkError
  }) => {
    if (
      graphQLErrors &&
      graphQLErrors[0].extensions &&
      graphQLErrors[0].extensions.code === 'invalid-jwt'
    ) {
      store.commit('common/logout')
      sessionStorage.removeItem('session')
      // app.$toast.error('session expired! please login')
      // app.router.replace('/')
      return
    }

    if (networkError) {
      // app.$toast.error('network error! please check your connection')
      console.log("network eror occured");

    }
  })

  const moviescheduleHTTPLink = new HttpLink({
    uri: "http://localhost:8080/v1/graphql"
  })

  const moviescheduleWSLink = new WebSocketLink({
    uri: "ws://localhost:8080/v1/graphql",
    options: {
      reconnect: true,
      lazy: true,
      connectionParams
    }
  })

  let authLink = (operation, forward) => {
    const {
      headers
    } = operation.getContext()
    if (store.getters['common/logged_in']) {
      operation.setContext({
        headers: {
          authorization: `Bearer ${store.getters['common/token']}`,
          // "x-hasura-admin-secret": 'secret-keys-are-must-be-long-and-long-secret-keys-are-must-be-long-and-long',
          ...headers
        }
      })
    }
    return forward(operation)
  }

  const getDefinition = ({
    query
  }) => {
    const definition = getMainDefinition(query)
    return (
      definition.kind === 'OperationDefinition' &&
      definition.operation === 'subscription'
    )
  }

  const moviescheduleLink = split(getDefinition, moviescheduleWSLink, moviescheduleHTTPLink)

  authLink = new ApolloLink(authLink)

  const moviescheduleApolloClient = new ApolloClient({
    link: from([errorLink, authLink, moviescheduleLink]),
    cache: new InMemoryCache({
      addTypename: false
    }),
    connectToDevTools: true
  })

  const apolloProvider = new VueApollo({
    defaultClient: moviescheduleApolloClient,
    clients: {
      movieschedule: moviescheduleApolloClient
    }
  })

  Vue.use(VueApollo)

  Vue.mixin({
    apolloProvider
  })
}
