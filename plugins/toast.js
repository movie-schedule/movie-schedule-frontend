export default function({ app }) {
  app.$toast.register('saved', app.i18n.t('saved'), {
    type: 'success'
  })
  app.$toast.register('removed', app.i18n.t('removed'), {
    type: 'success'
  })
}
