export const state = () => ({
  cinemaData: undefined,
  token: undefined
});

export const mutations = {
  setCinema(state, data) {
    state.cinemaData = data;
  },
  setToken(state, token) {
    state.token = token
  },
  updateCinema(state, data) {
    state.cinemaData.name = data.name
    state.cinemaData.email = data.email
    state.cinemaData.address = data.address
  },
  logOut(state) {
    state.cinemaData = {}
    state.token = undefined
  }
};

export const actions = {
  setCinema({
    commit
  }, data) {
    commit("setCinema", data);
    commit("setToken", data.token)
  },
  updateCinema({
    commit
  }, data) {
    commit("updateCinema", data)
  },
  logOut({
    commit
  }) {
    commit("logOut");
  }
};

export const getters = {
  logged_in(state) {
    return state.cinemaData !== undefined && state.token !== undefined
  },
  token(state) {
    return state.token
  },
  getCinemaInfo(state) {
    return state.cinemaData;
  }
};
