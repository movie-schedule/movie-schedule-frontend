import me from '~/queries/me.gql'
export const state = () => ({
  user: undefined,
  profile: undefined,
  userData: undefined
})

export const getters = {
  userData(state) {
    return state.userData || {}
  },
  token(state) {
    return state.user ? state.user.access_token : undefined
  },
  logged_in(state) {
    return state.user !== undefined && state.user !== null
  },
  id(state) {
    return state.user.metadata['x-hasura-user-id']
  },
  has_role: (state) => (role) => {
    return state.user.metadata['x-hasura-allowed-roles'].includes(role)
  },
  profile(state) {
    return state.profile || {}
  }
}

export const mutations = {
  set_pro_pic(state, pic) {
    state.userData.image_path = pic
  },
  set_userData(state, userdata) {
    state.userData = userdata
  },
  set_user_data(state, profile) {
    state.profile = profile
  },
  login(state, user) {
    state.user = user
  },
  logout(state) {
    state.user = undefined
    state.profile = undefined
    state.userData = undefined
    localStorage.removeItem('CMP')
    localStorage.removeItem('session')
    localStorage.removeItem('profile')
  }
}

export const actions = {
  logout({
    commit
  }) {
    commit("logout");
  },
  async fetch_user_data(ctx) {
    try {
      // console.log("hello hello hello");

      const {
        commit,
        getters
      } = ctx
      setTimeout(async () => {
        const client = this.$router.app.$apolloProvider.defaultClient
        const {
          data
        } = await client.query({
          query: me,
          manual: true,
          context: {
            headers: {
              authorization: `Bearer ${getters.token}`
            }
          },
          fetchPolicy: 'network-only'
        })
        if (data.me) {
          localStorage.setItem('profile', JSON.stringify(data.me))
          // console.log("me data", data.me);

          commit('set_user_data', data.me)
        }
      }, 1000)
    } catch (error) {
      console.log("error in common fetch", error);

    }

  }
}
