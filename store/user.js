export const state = () => ({
  userInfo: {}
});

export const mutations = {
  setUser(state, data) {
    state.userInfo = data;
  },
  logOut(state) {
    state.userInfo = {}
  }
};

export const actions = {
  setUserInfo({
    commit
  }, data) {
    commit("setUser", data);
  },
  updateUserInfo({
    commit
  }, data) {
    //updateuser
  },
  logOut({
    commit
  }) {
    commit("logOut")
  }
};

export const getters = {
  token(state) {
    return state.userInfo.token
  },
  getUserInfo(state) {
    return state.userInfo
  }
}
