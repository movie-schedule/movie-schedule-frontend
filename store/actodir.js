export const state = () => ({
  actors: [],
  directors: []
});

export const mutations = {
  setActors(state, data) {
    state.actors = data;
  },
  setDirectors(state, data) {
    state.directors = data
  },
  resetData(state) {


    state.actors = []
    state.directors = []
    console.log("logged out lo");
  },
  saveActor(state, actorData) {
    state.actors.push(actorData);
  },
  saveDirector(state, directorData) {
    state.directors.push(directorData);
  },
  updateActor(state, upData) {
    let id = upData.id
    let index = state.actors.findIndex((act) => act.id === id)

    state.actors.splice(index, 1, upData);
  },
  deleteActor(state, actorId) {
    let idx = state.actors.findIndex((actor) => actor.id === actorId);
    state.actors.splice(idx, 1)
  },
  updateDirector(state, upData) {
    let id = upData.id
    let index = state.directors.findIndex((dir) => dir.id === id)

    state.directors.splice(index, 1, upData);
  },
  deleteDirector(state, directorId) {
    let idx = state.directors.findIndex((director) => director.id === directorId);
    state.directors.splice(idx, 1)
  },
  logout(state) {
    state.actors = null
    state.directors = null
  }
};

export const actions = {
  setActors({
    commit
  }, data) {
    commit("setActors", data);
  },
  setDirectors({
    commit
  }, data) {
    commit("setDirectors", data);
  },
  saveActor({
    commit
  }, actData) {
    commit("saveActor", actData)
  },
  saveDirector({
    commit
  }, dirData) {
    commit("saveDirector", dirData)
  },
  updateActor({
    commit
  }, upData) {
    commit("updateActor", upData)
  },
  updateDirector({
    commit
  }, upData) {
    commit("updateDirector", upData)
  },
  deleteActor({
    commit
  }, actId) {
    commit("deleteActor", actId)
  },
  deleteDirector({
    commit
  }, dirId) {
    commit("deleteDirector", dirId)
  },
  resetData({
    commit
  }) {
    commit("resetData");
  }
};

export const getters = {
  getActors(state) {
    return state.actors
  },
  getDirectors(state) {
    return state.directors
  }
};
