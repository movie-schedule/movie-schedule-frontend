export const state = () => ({
  movies: [],
  searchedMovie: {},
  favoriteMovies: []
});

export const mutations = {
  setMovies(state, data) {
    state.movies = data;
  },
  setFavMovies(state, data) {
    state.favoriteMovies = data
  },
  setSearchedMovie(state, data) {
    state.searchedMovie = data;
  },
  saveMovie(state, movieData) {
    state.movies.push(movieData)
  },
  updateMovie(state, upData) {
    let id = upData.id
    let index = state.movies.findIndex((movie) => movie.id === id)

    state.movies.splice(index, 1, upData);
  },
  updateMovieRating(state, upData) {
    let id = upData.movieid
    let index = state.movies.findIndex((movie) => movie.id === id)

    state.movies[index].rating = upData.rating;
  },
  updateActorRating(state, upData) {
    let id = upData.movieid
    let actid = upData.actorid
    let index = state.movies.findIndex((movie) => movie.id === id)

    let actIndex = state.movies[index].movie_actors.findIndex(actor => actor.actor.id === actid)
    state.movies[index].movie_actors[actIndex].actor.rating = upData.rating;
  },
  updateDirectorRating(state, upData) {
    let id = upData.movieid
    let index = state.movies.findIndex((movie) => movie.id === id)

    state.movies[index].director.rating = upData.rating;
  },
  resetMovies(state) {
    state.movies = []
  },
  deleteMovie(state, movieId) {

    let idx = state.movies.findIndex((movie) => movie.id === movieId);
    state.movies.splice(idx, 1)
    // console.log(state.movies);

  },
  addImage(state, data) {
    let idx = state.movies.findIndex((movie) => movie.id === data.movieId);
    state.movies[idx].imagesByMovieid.push(data.picD)
  },
  deletePhoto(state, data) {
    let idx = state.movies.findIndex((movie) => movie.id === data.movieid);
    let picidx = state.movies[idx].imagesByMovieid.findIndex(pic => pic.id === data.picid)
    state.movies[idx].imagesByMovieid.splice(picidx, 1)
  },
  addFav(state, data) {
    state.favoriteMovies.push(data);
  },
  removeFav(state, id) {
    let idx = state.favoriteMovies.findIndex((fav) => fav.id === id);
    state.favoriteMovies.splice(idx, 1)
  },
  logout(state) {
    state.movies = []
    state.searchedMovie = {}
    state.favoriteMovies = []
  }
};

export const actions = {
  setFavMovies({
    commit
  }, data) {

    if (data) {
      localStorage.setItem('favoriteMovies', JSON.stringify(data))
    }
    commit("setFavMovies", data)
  },
  setMovies({
    commit
  }, data) {
    if (data) {
      localStorage.setItem('movies', JSON.stringify(data))
    }
    commit("setMovies", data);
  },
  setSearchedMovie({
    commit
  }, data) {
    commit("setSearchedMovie", data);
  },
  saveMovie({
    commit
  }, movieData) {
    commit("saveMovie", movieData)
  },
  updateMovie({
    commit
  }, upData) {
    commit("updateMovie", upData)
  },
  updateMovieRating({
    commit
  }, upData) {
    commit("updateMovieRating", upData);
  },
  updateActorRating({
    commit
  }, upData) {
    commit("updateActorRating", upData)
  },
  updateDirectorRating({
    commit
  }, upData) {
    commit("updateDirectorRating", upData)
  },
  deleteMovie({
    commit
  }, movieId) {
    commit("deleteMovie", movieId)
  },
  addImage({
    commit
  }, data) {
    commit("addImage", data)
  },
  deletePhoto({
    commit
  }, data) {
    commit("deletePhoto", data)
  },
  addFav({
    commit
  }, data) {
    commit("addFav", data)
  },
  removeFav({
    commit
  }, id) {
    commit("removeFav", id)
  },
  resetMovies({
    commit
  }) {
    commit("resetMovies");
  }
};

export const getters = {
  getMoviesCount(state) {
    return state.movies.length;
  },
  getDrafts(state) {
    let draftMovie = [];
    state.movies.forEach((mov) => {
      if (!mov.published) {
        let data = {
          id: mov.id,
          title: mov.title,
          actors: mov.movie_actors,
          director: mov.director,
          schedule: mov.schedule,
          published: mov.published,
          genre: mov.genre,
          movieLength: mov.movieLength,
          rating: mov.rating,
          imagesByMovieid: mov.imagesByMovieid
        }
        draftMovie.push(data)
      }

    })

    return draftMovie;
  },
  getDraftCount(state) {
    let drMv = [];
    state.movies.forEach((mov) => {
      if (!mov.published) {
        drMv.push(mov);
      }
    })
    return drMv.length
  },
  getLoadedMovies(state) {
    let publishedMovie = [];
    state.movies.forEach((mov) => {
      if (mov.published) {
        let data = {
          id: mov.id,
          title: mov.title,
          actors: mov.movie_actors,
          director: mov.director,
          schedule: mov.schedule,
          genre: mov.genre,
          movieLength: mov.movieLength,
          published: mov.published,
          rating: mov.rating,
          imagesByMovieid: mov.imagesByMovieid
        }
        publishedMovie.push(data)
      }

    })

    return publishedMovie;
  },
  getMovies(state) {
    return state.movies;
  },
  getSearchedMovie(state) {
    console.log("why the hell");
    console.log("in hell", state.searchedMovie);

    return state.searchedMovie;
  },
  getFavMovies(state) {
    return state.favoriteMovies
  }
};
